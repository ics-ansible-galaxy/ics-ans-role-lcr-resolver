ics-ans-role-lcr-resolver
===================

Ansible role to install lcr-resolver.

This role disable the /etc/resolv.conf management from DHCP and NetworkManager

Requirements
------------

- ansible >= 2.4
- molecule >= 2.6

Role Variables
--------------

```yaml
...
```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-lcr-resolver
```

License
-------

BSD 2-clause
